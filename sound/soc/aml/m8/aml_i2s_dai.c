#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/platform_device.h>
#include <linux/init.h>
#include <linux/errno.h>
#include <linux/ioctl.h>
#include <linux/delay.h>
#include <linux/slab.h>
#include <linux/dma-mapping.h>
#include <linux/soundcard.h>
#include <linux/timer.h>
#include <linux/debugfs.h>
#include <linux/major.h>

#include <sound/core.h>
#include <sound/pcm.h>
#include <sound/initval.h>
#include <sound/control.h>
#include <sound/soc.h>
#include <sound/pcm_params.h>
#include <sound/tlv.h>
#include <mach/am_regs.h>
#include <mach/pinmux.h>
#include "aml_i2s_dai.h"
#include "aml_pcm.h"
#include "aml_i2s.h"
#include "aml_audio_hw.h"
#include <linux/of.h>

static aml_dai_info_t dai_info[3] = { {0} };

static int i2s_pos_sync;

struct channel_speaker_allocation {
        int channels;
        int speakers[8];
};

#define NA	SNDRV_CHMAP_NA
#define FL	SNDRV_CHMAP_FL
#define FR	SNDRV_CHMAP_FR
#define RL	SNDRV_CHMAP_RL
#define RR	SNDRV_CHMAP_RR
#define LFE	SNDRV_CHMAP_LFE
#define FC	SNDRV_CHMAP_FC
#define RLC	SNDRV_CHMAP_RLC
#define RRC	SNDRV_CHMAP_RRC
#define RC	SNDRV_CHMAP_RC
#define FLC	SNDRV_CHMAP_FLC
#define FRC	SNDRV_CHMAP_FRC
#define FLH	SNDRV_CHMAP_TFL
#define FRH	SNDRV_CHMAP_TFR
#define FLW	SNDRV_CHMAP_FLW
#define FRW	SNDRV_CHMAP_FRW
#define TC	SNDRV_CHMAP_TC
#define FCH	SNDRV_CHMAP_TFC

static struct channel_speaker_allocation channel_allocations[] = {
/*      	       channel:   7     6    5    4    3     2    1    0  */
{ .channels = 2,  .speakers = {  NA,   NA,  NA,  NA,  NA,   NA,  FR,  FL } },
                                 /* 2.1 */
{ .channels = 3,  .speakers = {  NA,   NA,  NA,  NA,  NA,  LFE,  FR,  FL } },
                                 /* surround40 */
{ .channels = 4,  .speakers = {  NA,   NA,  RR,  RL,  NA,   NA,  FR,  FL } },
                                 /* surround41 */
{ .channels = 5,  .speakers = {  NA,   NA,  RR,  RL,  NA,  LFE,  FR,  FL } },
                                 /* surround50 */
{ .channels = 5,  .speakers = {  NA,   NA,  RR,  RL,  FC,   NA,  FR,  FL } },
                                 /* surround51 */
{ .channels = 6,  .speakers = {  NA,   NA,  RR,  RL,  FC,  LFE,  FR,  FL } },
                                 /* 6.1 */
{ .channels = 7,  .speakers = {  NA,   RC,  RR,  RL,  FC,  LFE,  FR,  FL } },
                                 /* surround71 */
{ .channels = 8,  .speakers = { RRC,  RLC,  RR,  RL,  FC,  LFE,  FR,  FL } },
};

/* #define AML_DAI_DEBUG */
#define ALSA_PRINT(fmt, args...)	printk(KERN_INFO "[aml-i2s-dai]" fmt, ##args)
#ifdef AML_DAI_DEBUG
#define ALSA_DEBUG(fmt, args...) 	printk(KERN_INFO "[aml-i2s-dai]" fmt, ##args)
#define ALSA_TRACE()     			printk("[aml-i2s-dai] enter func %s,line %d\n", __func__, __LINE__)
#else
#define ALSA_DEBUG(fmt, args...)
#define ALSA_TRACE()
#endif
extern int amaudio2_enable;
extern unsigned int IEC958_mode_codec;

extern int set_i2s_iec958_samesource(int enable);

static int i2sbuf[32 + 16];
static void aml_i2s_play(void)
{
	audio_util_set_dac_i2s_format(AUDIO_ALGOUT_DAC_FORMAT_DSP);
	audio_set_i2s_mode(AIU_I2S_MODE_PCM16);
	memset(i2sbuf, 0, sizeof(i2sbuf));
	audio_set_aiubuf((virt_to_phys(i2sbuf) + 63) & (~63), 128, 2);
	audio_out_i2s_enable(1);
}

/*
the I2S hw  and IEC958 PCM output initation,958 initation here,
for the case that only use our ALSA driver for PCM s/pdif output.
*/
static void aml_hw_i2s_init(struct snd_pcm_runtime *runtime)
{
	unsigned i2s_mode = AIU_I2S_MODE_PCM16;
	switch (runtime->format) {
	case SNDRV_PCM_FORMAT_S32_LE:
		i2s_mode = AIU_I2S_MODE_PCM32;
		break;
	case SNDRV_PCM_FORMAT_S24_LE:
		i2s_mode = AIU_I2S_MODE_PCM24;
		break;
	case SNDRV_PCM_FORMAT_S16_LE:
		i2s_mode = AIU_I2S_MODE_PCM16;
		break;
	}
	audio_set_i2s_mode(i2s_mode);
	audio_set_aiubuf(runtime->dma_addr, runtime->dma_bytes,
			 runtime->channels);
	
ALSA_PRINT("i2s dma %x,phy addr %x,mode %d,ch %d \n",
		    (unsigned)runtime->dma_area, (unsigned)runtime->dma_addr,
		    i2s_mode, runtime->channels);

}

static int aml_dai_i2s_chmap_ctl_tlv(struct snd_kcontrol *kcontrol, int op_flag,
                                     unsigned int size, unsigned int __user *tlv)
{
    unsigned int __user *dst;
    int count = 0;
    int i;

    if (size < 8)
        return -ENOMEM;

    if (put_user(SNDRV_CTL_TLVT_CONTAINER, tlv))
        return -EFAULT;

    size -= 8;
    dst = tlv + 2;

    for (i = 0; i < ARRAY_SIZE(channel_allocations); i++)
    {
        struct channel_speaker_allocation *ch = &channel_allocations[i];
        int num_chs = 0;
        int chs_bytes;
        int c;

        for (c = 0; c < 8; c++)
        {
            if (ch->speakers[c])
                num_chs++;
        }

        chs_bytes = num_chs * 4;
        if (size < 8)
            return -ENOMEM;

        if (put_user(SNDRV_CTL_TLVT_CHMAP_FIXED, dst) ||
            put_user(chs_bytes, dst + 1))
            return -EFAULT;

        dst += 2;
        size -= 8;
        count += 8;

        if (size < chs_bytes)
            return -ENOMEM;

        size -= chs_bytes;
        count += chs_bytes;

        for (c = 0; c < 8; c++)
        {
            int sp = ch->speakers[7 - c];
            if (sp)
            {
                if (put_user(sp, dst))
                    return -EFAULT;
                dst++;
            }
        }
    }

    if (put_user(count, tlv + 1))
        return -EFAULT;

    return 0;
}

static int aml_dai_i2s_chmap_ctl_get(struct snd_kcontrol *kcontrol,
                                     struct snd_ctl_elem_value *ucontrol)
{

    struct snd_pcm_chmap *info = snd_kcontrol_chip(kcontrol);
    unsigned int idx = snd_ctl_get_ioffidx(kcontrol, &ucontrol->id);
    struct snd_pcm_substream *substream = snd_pcm_chmap_substream(info, idx);
    struct snd_pcm_runtime *runtime = substream->runtime;
    struct aml_runtime_data *prtd = (struct aml_runtime_data *)runtime->private_data;
    int res = 0, channel;

    if (mutex_lock_interruptible(&prtd->chmap_lock))
        return -EINTR;

    // we need 8 channels
    if (runtime->channels != 8)
    {
        pr_err("channel count should be 8, we got %d aborting\n", runtime->channels);
        res = -EINVAL;
        goto unlock;
    }

    for (channel=0; channel<8; channel++)
    {
        ucontrol->value.integer.value[7 - channel] = channel_allocations[prtd->chmap_layout].speakers[channel];
    }

unlock:
    mutex_unlock(&prtd->chmap_lock);
    return res;
}

static int aml_dai_i2s_chmap_ctl_put(struct snd_kcontrol *kcontrol,
                                     struct snd_ctl_elem_value *ucontrol)
{

    struct snd_pcm_chmap *info = snd_kcontrol_chip(kcontrol);
    unsigned int idx = snd_ctl_get_ioffidx(kcontrol, &ucontrol->id);
    struct snd_pcm_substream *substream = snd_pcm_chmap_substream(info, idx);
    struct snd_pcm_runtime *runtime = substream->runtime;
    struct aml_runtime_data *prtd = (struct aml_runtime_data *)runtime->private_data;
    int res = 0, channel, layout, matches, matched_layout;

    if (mutex_lock_interruptible(&prtd->chmap_lock))
        return -EINTR;

    // we need 8 channels
    if (runtime->channels != 8)
    {
        pr_err("channel count should be 8, we got %d aborting\n", runtime->channels);
        res = -EINVAL;
        goto unlock;
    }

    // now check if the channel setup matches one of our layouts
    for (layout = 0; layout < ARRAY_SIZE(channel_allocations); layout++)
    {
        matches = 1;

        for (channel = 0; channel < substream->runtime->channels; channel++)
        {
            int sp = ucontrol->value.integer.value[channel];
            int chan = channel_allocations[layout].speakers[7 - channel];

            if (sp != chan)
            {
                matches = 0;
                break;
            }
        }

        if (matches)
        {
            matched_layout = layout;
            break;
        }
    }


    // default to first layout if we didnt find any
    if (!matches)
        matched_layout = 0;

    pr_info("Setting a %d channel layout matching layout #%d\n", runtime->channels, matched_layout);

    prtd->chmap_layout = matched_layout;

unlock:
    mutex_unlock(&prtd->chmap_lock);
    return res;
}

static struct snd_kcontrol *aml_dai_i2s_chmap_kctrl_get(struct snd_pcm_substream *substream)
{
    int str;

    if ((substream) && (substream->pcm))
    {
        for (str=0; str<2; str++)
        {
            if (substream->pcm->streams[str].chmap_kctl)
            {
                return substream->pcm->streams[str].chmap_kctl;
            }
        }
    }

    return 0;
}

static int aml_dai_i2s_startup(struct snd_pcm_substream *substream,
			       struct snd_soc_dai *dai)
{
	int ret = 0, i;
	struct snd_pcm_runtime *runtime = substream->runtime;
	struct aml_runtime_data *prtd =
	    (struct aml_runtime_data *)runtime->private_data;
	audio_stream_t *s;
	struct snd_pcm_chmap *chmap;
	struct snd_kcontrol *kctl;
	ALSA_TRACE();
	if (prtd == NULL) {
		prtd =
		    (struct aml_runtime_data *)
		    kzalloc(sizeof(struct aml_runtime_data), GFP_KERNEL);
		if (prtd == NULL) {
			printk("alloc aml_runtime_data error\n");
			ret = -ENOMEM;
			goto out;
		}
		prtd->substream = substream;
		runtime->private_data = prtd;
	}
	s = &prtd->s;
	if (substream->stream == SNDRV_PCM_STREAM_PLAYBACK) {
		s->device_type = AML_AUDIO_I2SOUT;
	} else {
		s->device_type = AML_AUDIO_I2SIN;
	}

	// Alsa Channel Mapping API handling
	if (!aml_dai_i2s_chmap_kctrl_get(substream))
	{
	    ret = snd_pcm_add_chmap_ctls(substream->pcm, SNDRV_PCM_STREAM_PLAYBACK, NULL, 8, 0, &chmap);

	    if (ret < 0)
	    {
	      pr_err("aml_dai_i2s_startup error %d\n", ret);
	      goto out;
	    }

	    kctl = chmap->kctl;
	    for (i = 0; i < kctl->count; i++)
	      kctl->vd[i].access |= SNDRV_CTL_ELEM_ACCESS_WRITE;

	    kctl->get = aml_dai_i2s_chmap_ctl_get;
	    kctl->put = aml_dai_i2s_chmap_ctl_put;
	    kctl->tlv.c = aml_dai_i2s_chmap_ctl_tlv;
	}

	mutex_init(&prtd->chmap_lock);
	return 0;
 out:
	return ret;
}

extern void aml_spdif_play(void);
static void aml_dai_i2s_shutdown(struct snd_pcm_substream *substream,
				 struct snd_soc_dai *dai)
{
	struct snd_pcm_runtime *runtime = substream->runtime;
	if (runtime->channels == 8 || IEC958_mode_codec == 0) {
		aml_spdif_play();
	}
}

#define AOUT_EVENT_IEC_60958_PCM 0x1
extern int aout_notifier_call_chain(unsigned long val, void *v);

extern void aml_hw_iec958_init(struct snd_pcm_substream *substream);

static int aml_dai_i2s_prepare(struct snd_pcm_substream *substream,
			       struct snd_soc_dai *dai)
{
	struct snd_pcm_runtime *runtime = substream->runtime;
	struct aml_runtime_data *prtd = runtime->private_data;
	struct aml_i2s *i2s = snd_soc_dai_get_drvdata(dai);
	int audio_clk_config = AUDIO_CLK_FREQ_48;
	audio_stream_t *s = &prtd->s;
	ALSA_TRACE();
	switch (runtime->rate) {
	case 192000:
		audio_clk_config = AUDIO_CLK_FREQ_192;
		break;
	case 176400:
		audio_clk_config = AUDIO_CLK_FREQ_1764;
		break;
	case 96000:
		audio_clk_config = AUDIO_CLK_FREQ_96;
		break;
	case 88200:
		audio_clk_config = AUDIO_CLK_FREQ_882;
		break;
	case 48000:
		audio_clk_config = AUDIO_CLK_FREQ_48;
		break;
	case 44100:
		audio_clk_config = AUDIO_CLK_FREQ_441;
		break;
	case 32000:
		audio_clk_config = AUDIO_CLK_FREQ_32;
		break;
	case 8000:
		audio_clk_config = AUDIO_CLK_FREQ_8;
		break;
	case 11025:
		audio_clk_config = AUDIO_CLK_FREQ_11;
		break;
	case 16000:
		audio_clk_config = AUDIO_CLK_FREQ_16;
		break;
	case 22050:
		audio_clk_config = AUDIO_CLK_FREQ_22;
		break;
	case 12000:
		audio_clk_config = AUDIO_CLK_FREQ_12;
		break;
	case 24000:
		audio_clk_config = AUDIO_CLK_FREQ_22;
		break;
	default:
		audio_clk_config = AUDIO_CLK_FREQ_441;
		break;
	};
	if (substream->stream == SNDRV_PCM_STREAM_PLAYBACK)
		audio_out_i2s_enable(0);
	if (i2s->old_samplerate != runtime->rate) {
		ALSA_PRINT("enterd %s,old_samplerate:%d,sample_rate=%d\n",
			   __func__, i2s->old_samplerate, runtime->rate);
		
i2s->old_samplerate = runtime->rate;
		audio_set_i2s_clk(audio_clk_config, AUDIO_CLK_256FS, i2s->mpll);
	}
	audio_util_set_dac_i2s_format(AUDIO_ALGOUT_DAC_FORMAT_DSP);

	if (substream->stream == SNDRV_PCM_STREAM_CAPTURE) {
		s->i2s_mode = dai_info[dai->id].i2s_mode;
		audio_in_i2s_set_buf(runtime->dma_addr, runtime->dma_bytes * 2,
				     0, i2s_pos_sync);
		
memset((void *)runtime->dma_area, 0, runtime->dma_bytes * 2);
		
 {
			int *ppp =
			    (int *)(runtime->dma_area + runtime->dma_bytes * 2 -
				    8);
			ppp[0] = 0x78787878;
			ppp[1] = 0x78787878;
		}
		s->device_type = AML_AUDIO_I2SIN;
	} else {
		s->device_type = AML_AUDIO_I2SOUT;
		aml_hw_i2s_init(runtime);
/* i2s/958 share the same audio hw buffer when PCM mode */
		if (IEC958_mode_codec == 0) {
			aml_hw_iec958_init(substream);
			/* use the hw same sync for i2s/958 */
			WRITE_MPEG_REG_BITS(AIU_I2S_MISC, 1, 3, 1);
		}
		else
			WRITE_MPEG_REG_BITS(AIU_I2S_MISC, 0, 3, 1);
	}
	if (runtime->channels == 8) {
		ALSA_PRINT("[%s,%d]8ch PCM output->notify HDMI\n", __func__,
			   __LINE__);
		
aout_notifier_call_chain(AOUT_EVENT_IEC_60958_PCM, substream);
	
}
	return 0;
}

static int aml_dai_i2s_trigger(struct snd_pcm_substream *substream, int cmd,
			       struct snd_soc_dai *dai)
{
	struct snd_pcm_runtime *rtd = substream->runtime;
	int *ppp = NULL;
	ALSA_TRACE();
	switch (cmd) {
	case SNDRV_PCM_TRIGGER_START:
	case SNDRV_PCM_TRIGGER_RESUME:
	case SNDRV_PCM_TRIGGER_PAUSE_RELEASE:
		/* TODO */
		if (substream->stream == SNDRV_PCM_STREAM_PLAYBACK) {
			ALSA_PRINT("aiu i2s playback enable\n\n");
			audio_out_i2s_enable(1);
			if (IEC958_mode_codec == 0) {
				ALSA_PRINT("audio_hw_958_enable  1\n");
				audio_hw_958_enable(1);
			}
		} else {
			audio_in_i2s_enable(1);
			ppp = (int *)(rtd->dma_area + rtd->dma_bytes * 2 - 8);
			ppp[0] = 0x78787878;
			ppp[1] = 0x78787878;
		}
		break;
	case SNDRV_PCM_TRIGGER_STOP:
	case SNDRV_PCM_TRIGGER_SUSPEND:
	case SNDRV_PCM_TRIGGER_PAUSE_PUSH:
		if (substream->stream == SNDRV_PCM_STREAM_PLAYBACK) {
			ALSA_PRINT("aiu i2s playback disable\n\n");
			audio_out_i2s_enable(0);
			if (IEC958_mode_codec == 0) {
				
ALSA_PRINT("audio_hw_958_enable  0\n");
				audio_hw_958_enable(0);
			}
		} else {
			audio_in_i2s_enable(0);
		}
		break;
	default:
		return -EINVAL;
	}

	return 0;
}

static int aml_dai_i2s_hw_params(struct snd_pcm_substream *substream,
				 struct snd_pcm_hw_params *params,
				 struct snd_soc_dai *dai)
{
	ALSA_TRACE();
	return 0;
}

static int aml_dai_set_i2s_fmt(struct snd_soc_dai *dai, unsigned int fmt)
{
	ALSA_TRACE();
	if (fmt & SND_SOC_DAIFMT_CBS_CFS)	/* slave mode */
		dai_info[dai->id].i2s_mode = I2S_SLAVE_MODE;

	switch (fmt & SND_SOC_DAIFMT_INV_MASK) {
	case SND_SOC_DAIFMT_NB_NF:
		i2s_pos_sync = 0;
		break;
	case SND_SOC_DAIFMT_IB_NF:
		i2s_pos_sync = 1;
		break;
	default:
		return -EINVAL;
	}
	return 0;
}

static int aml_dai_set_i2s_sysclk(struct snd_soc_dai *dai,
				  int clk_id, unsigned int freq, int dir)
{
	ALSA_TRACE();
	return 0;
}

#ifdef CONFIG_PM
static int aml_dai_i2s_suspend(struct snd_soc_dai *dai)
{
	ALSA_TRACE();
	return 0;
}

static int aml_dai_i2s_resume(struct snd_soc_dai *dai)
{
	ALSA_TRACE();
	return 0;
}

#else				/* CONFIG_PM */
#define aml_dai_i2s_suspend	NULL
#define aml_dai_i2s_resume	NULL
#endif				/* CONFIG_PM */

#define AML_DAI_I2S_RATES	SNDRV_PCM_RATE_44100 | SNDRV_PCM_RATE_48000 | SNDRV_PCM_RATE_88200 | SNDRV_PCM_RATE_96000 | SNDRV_PCM_RATE_176400 | SNDRV_PCM_RATE_192000
#define AML_DAI_I2S_FORMATS	SNDRV_PCM_FMTBIT_S16_LE | SNDRV_PCM_FMTBIT_S32_LE

static struct snd_soc_dai_ops aml_dai_i2s_ops = {
	.startup = aml_dai_i2s_startup,
	.shutdown = aml_dai_i2s_shutdown,
	.prepare = aml_dai_i2s_prepare,
	.trigger = aml_dai_i2s_trigger,
	.hw_params = aml_dai_i2s_hw_params,
	.set_fmt = aml_dai_set_i2s_fmt,
	.set_sysclk = aml_dai_set_i2s_sysclk,
};

struct snd_soc_dai_driver aml_i2s_dai[] = {
	{.name = "aml-i2s-dai",
	 .id = 0,
	 .suspend = aml_dai_i2s_suspend,
	 .resume = aml_dai_i2s_resume,
	 .playback = {
		      .channels_min = 1,
		      .channels_max = 8,
		      .rates = AML_DAI_I2S_RATES,
		      .formats = AML_DAI_I2S_FORMATS,},
	 .capture = {
		     .channels_min = 1,
		     .channels_max = 8,
		     .rates = AML_DAI_I2S_RATES,
		     .formats = AML_DAI_I2S_FORMATS,},
	 .ops = &aml_dai_i2s_ops,
	 },

};

EXPORT_SYMBOL_GPL(aml_i2s_dai);

static const struct snd_soc_component_driver aml_component = { 
.name =
	    "aml-i2s-dai",
};

static int aml_i2s_dai_probe(struct platform_device *pdev)
{
	struct aml_i2s *i2s = NULL;
	int ret = 0;
	printk(KERN_DEBUG "enter %s\n", __func__);

	i2s = kzalloc(sizeof(struct aml_i2s), GFP_KERNEL);
	if (!i2s) {
		dev_err(&pdev->dev, "Can't allocate aml_i2s\n");
		ret = -ENOMEM;
		goto exit;
	}
	dev_set_drvdata(&pdev->dev, i2s);

	if (of_property_read_u32(pdev->dev.of_node, "clk_src_mpll", &i2s->mpll)) {
		printk(KERN_INFO
		       "i2s get no clk src setting in dts, use the default mpll 0\n");
		i2s->mpll = 0;
	}
	/* enable i2s MPLL and power gate first */
	WRITE_MPEG_REG_BITS(MPLL_I2S_CNTL, 1, 14, 1);
	audio_aiu_pg_enable(1);
	/* enable the mclk because m8 codec need it to setup */
	if (i2s->old_samplerate != 48000) {
		ALSA_PRINT("enterd %s,old_samplerate:%d,sample_rate=%d\n",
			   __func__, i2s->old_samplerate, 48000);
		
i2s->old_samplerate = 48000;
		audio_set_i2s_clk(AUDIO_CLK_FREQ_48, AUDIO_CLK_256FS,
				  i2s->mpll);
	}
	aml_i2s_play();
	return snd_soc_register_component(&pdev->dev, &aml_component,
					  aml_i2s_dai, ARRAY_SIZE(aml_i2s_dai));

 exit:
	return ret;
}

static int aml_i2s_dai_remove(struct platform_device *pdev)
{
	struct aml_i2s *i2s = dev_get_drvdata(&pdev->dev);

	snd_soc_unregister_component(&pdev->dev);
	kfree(i2s);
	i2s = NULL;

	return 0;
}

#ifdef CONFIG_USE_OF
static const struct of_device_id amlogic_dai_dt_match[] = {
	    {.compatible = "amlogic,aml-i2s-dai",
	     },
{},
};
#else
#define amlogic_dai_dt_match NULL
#endif

static struct platform_driver aml_i2s_dai_driver = {
	.driver = {
		   .name = "aml-i2s-dai",
		   .owner = THIS_MODULE,
		   .of_match_table = amlogic_dai_dt_match,
		   },

	.probe = aml_i2s_dai_probe,
	.remove = aml_i2s_dai_remove,
};

static int __init aml_i2s_dai_modinit(void)
{
	return platform_driver_register(&aml_i2s_dai_driver);
}

module_init(aml_i2s_dai_modinit);

static void __exit aml_i2s_dai_modexit(void)
{
	platform_driver_unregister(&aml_i2s_dai_driver);
}

module_exit(aml_i2s_dai_modexit);

/* Module information */
MODULE_AUTHOR("AMLogic, Inc.");
MODULE_DESCRIPTION("AML DAI driver for ALSA");
MODULE_LICENSE("GPL");
