/*
 *  linux/drivers/amlogic/gpio-sysled/gpio-sysled.c
 *
 *  Copyright (C) 2017 Stanislav Vlasic, All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#include <linux/err.h>
#include <linux/ion.h>
#include <linux/platform_device.h>
#include <linux/slab.h>
#include <linux/module.h>
#include <linux/netdevice.h>
#include <linux/notifier.h>
#include <linux/of.h>
#include <linux/of_fdt.h>
#include <linux/string.h>
#include <linux/amlogic/aml_gpio_consumer.h>

MODULE_DESCRIPTION("Amlogic Sysled driver");
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Stanislav Vlasic");

#define GPIO_SYSLED_OWNER "GPIO_SYSLED"

// Default values for power led GPIO
static int POWER_LED_GPIO = -1;
static int POWER_LED_STATUS_ON = 1;
static int POWER_LED_STATUS_OFF = 0;

// Default values for net status led GPIO
static int NET_STATUS_GPIO = -1;
static int NET_STATUS_ON = 1;
static int NET_STATUS_OFF = 0;

#ifdef CONFIG_HAS_EARLYSUSPEND
#include <linux/earlysuspend.h>
static struct early_suspend early_suspend;
#endif

static void net_status_control(int value);
static void power_status_control(int value);
static int gpio_sysled_net_device_notification(struct notifier_block *, ulong, void *);

// Value for storing currently active device name
static char* cur_device;

/* notification function for net device */
static struct notifier_block gpio_sysled_net_notifier = {
		.notifier_call = gpio_sysled_net_device_notification,
};

int gpio_sysled_probe(struct platform_device *pdev) {
	int ret_dts = -1;
	const char *str;
	int value = -1;
	struct net_device *dev;

	ret_dts = of_property_read_string(pdev->dev.of_node, "power-led-gpio", &str);
	if (ret_dts) {
		printk("Error: can not get power-led-gpio\n");
		POWER_LED_GPIO = -1;
	}
	else {
		POWER_LED_GPIO = amlogic_gpio_name_map_num(str);
		amlogic_gpio_request(POWER_LED_GPIO, GPIO_SYSLED_OWNER);
		printk("power-led-gpio is %d\n", POWER_LED_GPIO);

		// Read power led ON value
		value=-1;
		ret_dts=of_property_read_u32(pdev->dev.of_node, "power-led-on", &value);
		if (ret_dts)
		{
			printk("Error: no power-led-on defined, using default value: %d\n", POWER_LED_STATUS_ON);
		}
		else {
			POWER_LED_STATUS_ON = value;
		}

		// Read power led OFF value
		value=-1;
		ret_dts=of_property_read_u32(pdev->dev.of_node, "power-led-off", &value);
		if (ret_dts)
		{
			printk("Error: no power-led-off defined, using default value: %d\n", POWER_LED_STATUS_OFF);
		}
		else {
			POWER_LED_STATUS_OFF = value;
		}

		// Trigger power status now
		power_status_control(POWER_LED_STATUS_ON);
	}

	// Check if we also control net status led
	ret_dts = of_property_read_string(pdev->dev.of_node, "net-led-gpio", &str);
	if (ret_dts)
	{
		printk("Error: can not get net-led-gpio\n");
		NET_STATUS_GPIO = -1;
	} else {
		NET_STATUS_GPIO = amlogic_gpio_name_map_num(str);
		amlogic_gpio_request(NET_STATUS_GPIO, GPIO_SYSLED_OWNER);
		printk("net-led-gpio is %d\n", NET_STATUS_GPIO);

		// Read network led ON value
		value=-1;
		ret_dts=of_property_read_u32(pdev->dev.of_node, "net-led-on", &value);
		if (ret_dts)
		{
			printk("Error: no net-led-on defined, using default value: %d\n", NET_STATUS_ON);
		}
		else {
			NET_STATUS_ON = value;
		}
		// Read network led OFF value
		value=-1;
		ret_dts=of_property_read_u32(pdev->dev.of_node, "net-led-off", &value);
		if (ret_dts)
		{
			printk("Error: no net-led-off defined, using default value: %d\n", NET_STATUS_OFF);
		}
		else {
			NET_STATUS_OFF = value;
		}
	}

	// If we found network led  GPIO, register network notifier
	// and scan for running interfaces
	if (NET_STATUS_GPIO > -1) {
		register_netdevice_notifier(&gpio_sysled_net_notifier);

		read_lock(&dev_base_lock);
		dev = first_net_device(&init_net);
		int flags;
		while (dev) {
			if ((strcmp(dev->name, "eth0") == 0) || (strcmp(dev->name, "wlan0") == 0)) {
				flags = dev_get_flags(dev);
				if ((flags >= 0) && (flags & IFF_RUNNING)) {
					cur_device = dev->name;
					net_status_control(NET_STATUS_ON);
					break;
				}
			}
			dev = next_net_device(dev);
		}
		read_unlock(&dev_base_lock);
	}
	return 0;
}

int gpio_sysled_remove(struct platform_device *pdev) {
        if (POWER_LED_GPIO > -1)
                power_status_control(POWER_LED_STATUS_OFF);

        if (NET_STATUS_GPIO > -1) {
                net_status_control(NET_STATUS_OFF);
		unregister_netdevice_notifier(&gpio_sysled_net_notifier);
	}
	return 0;
}

static void net_status_control(int value)
{
		amlogic_gpio_free(NET_STATUS_GPIO, GPIO_SYSLED_OWNER);
		amlogic_gpio_request(NET_STATUS_GPIO, GPIO_SYSLED_OWNER);
		amlogic_gpio_direction_output(NET_STATUS_GPIO, value, GPIO_SYSLED_OWNER);
}

static void power_status_control(int value)
{
		amlogic_gpio_free(POWER_LED_GPIO, GPIO_SYSLED_OWNER);
		amlogic_gpio_request(POWER_LED_GPIO, GPIO_SYSLED_OWNER);
		amlogic_gpio_direction_output(POWER_LED_GPIO, value, GPIO_SYSLED_OWNER);
}

/**
 * gpio_sysled_net_device_notification() - Handler for net device events
 * @notifier: The context of the notification
 * @event:	The type of event
 * @ptr:	  The net device that the event was on
 *
 * This function is called by the gpio-sysled driver in case of link change event.
 *
 * Returns: 0 for success
 */
static int gpio_sysled_net_device_notification(struct notifier_block *notifier,
									ulong event, void *ptr)
{
	struct net_device *dev = (struct net_device *)ptr;
	int flags;
	// TODO In future read devices to be checked from devie tree
	if ((strcmp(dev->name, "eth0") == 0) || (strcmp(dev->name, "wlan0") == 0)) {
		cur_device = dev->name;
		flags = dev_get_flags(dev);
		if ((flags != NULL) && (flags & IFF_RUNNING))
			net_status_control(NET_STATUS_ON);
		else
			net_status_control(NET_STATUS_OFF);
	}
	return 0;
}

static const struct of_device_id amlogic_gpio_sysled_dt_match[] = { { .compatible = "amlogic,gpio-sysled", }, { }, };

static struct platform_driver gpio_sysled_driver = {
		.probe = gpio_sysled_probe,
		.remove = gpio_sysled_remove,
		.driver = {
				.name = "gpio-sysled",
				.owner = THIS_MODULE,
				.of_match_table = amlogic_gpio_sysled_dt_match
		}
};

#ifdef CONFIG_HAS_EARLYSUSPEND
static void gpio_sysled_early_suspend(struct early_suspend *h)
{
	if (POWER_LED_GPIO > -1)
		power_status_control(POWER_LED_STATUS_OFF);

	if (NET_STATUS_GPIO > -1)
		net_status_control(NET_STATUS_OFF);
}

static void gpio_sysled_late_resume(struct early_suspend *h)
{
	if (POWER_LED_GPIO > -1)
		power_status_control(POWER_LED_STATUS_ON);

	if (NET_STATUS_GPIO > -1) {
		if (cur_device != NULL) {
			int flags = dev_get_flags(dev_get_by_name(&init_net, cur_device));
			if ((flags >= 0) && (flags & IFF_RUNNING))
				net_status_control(NET_STATUS_ON);
			else
				net_status_control(NET_STATUS_OFF);
		}
	}
}
#endif

static int __init gpio_sysled_init(void)
{
	printk("GPIO-SYSLED: Init driver\n");
#ifdef CONFIG_HAS_EARLYSUSPEND
	early_suspend.level = EARLY_SUSPEND_LEVEL_DISABLE_FB;
	early_suspend.suspend = gpio_sysled_early_suspend;
	early_suspend.resume = gpio_sysled_late_resume;
	register_early_suspend(&early_suspend);
#endif
	return platform_driver_register(&gpio_sysled_driver);
}

static void __exit gpio_sysled_exit(void)
{
	printk("GPIO-SYSLED: unload driver\n");
	if (POWER_LED_GPIO > -1)
		power_status_control(POWER_LED_STATUS_OFF);

	if (NET_STATUS_GPIO > -1)
		net_status_control(NET_STATUS_OFF);

#ifdef CONFIG_HAS_EARLYSUSPEND
	unregister_early_suspend(&early_suspend);
#endif
	platform_driver_unregister(&gpio_sysled_driver);
}

module_init(gpio_sysled_init);
module_exit(gpio_sysled_exit);
